# Overview
The Project has three stages and seeks to ultimately provide the ability to move everything around digitally.

## Stage 0 Genesis 
Up and running. Goal is to provide a mobile operating system that is available thru any browser on any device.

Expected Time: 25 Years

## Stage 1 Hydra
Protection in redundancy. Goal is to provide a network of mobile operating system providers that adhere to The Projects Specs and can be directly installed on hardware.

Expected Time: 15 Years

## Stage 2 Decentralization
Move it so The Project is no longer controlled by one person, group or individual. Do however is necessary.

Expected Time: 10 Years

## Stage 3 The Solution
With The Project having complete dominance on all operating systems, provide the ability to transfer people around the world, much like we can transmit images around the world. Complete Onlinification means that people can solve problems in many different ways. Time will become irrelevant, death no longer an issue.

Some forecasted ways:

1) Redundant Backup - If you die during your day, no worry. A clone will be created the next day with the knowledge and experiences you possessed up to your last backup.

2) Multi Copies - Creating multiple copies so that you can be in more than one location or solve a really difficult problem.

                - Information acquisition, with multi copies it would be possible for everyone to learn something that interests them, then come together and "synchronize" what they learned.

Problems that we must address:
Clones are separate entities, they are you as in they were you up to the moment they came online. After that they are themselves, except with your history. Therefore they must be given equal right. 

Upon cloning, they must be able to be supported or support themselves, they must be able to have all legal protection, privileges and rights that all other entities including humans or whatever we are or become or are replaced by.

"Dephysicalizing" - Converting an organic/physical object into a full digital representation. This means that they are granted the same rights as if they were physical beings. This just means less "physical resource usage". Like The Matrix except benign or like Corporation Wars by Ken MacLeod except benign.

The ultimate goal is to move all of humanity into a simulation where we are no longer using up a massive amount of earth's resources and can allow other creatures, plants and environments to flourish.

Expected Time: ∞
